const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '../.env') })

const express = require('express')
const passport = require('passport')
const bodyParser = require('body-parser')
// const cors = require('cors')
const apiRoot = process.env.API_ROOT
const port = process.env.API_PORT
const mongoose = require('mongoose')
const connString = process.env.CONN_STRING
const cookieParser = require('cookie-parser')

const app = express()

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  next()
})

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(passport.initialize())
app.use(passport.session())
app.use(cookieParser())

mongoose.connect(connString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
}, (err) => {
  if (err) {
    console.error(err)
  }
  console.log('MongoDB Connected.')
})

app.get(path.join(apiRoot, '/'), (req, res) => {
  res.json({
    status: 200,
    message: 'API Root'
  })
})

app.use(path.join(apiRoot, '/businesses'), require('./routes/business'))
app.use(path.join(apiRoot, '/auth'), require('./routes/auth'))
app.use(path.join(apiRoot, '/users'), require('./routes/users'))

app.listen(port, () => {
  console.log(`Server listening on ${port}`)
})
