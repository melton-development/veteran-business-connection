const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '../../.env') })

const express = require('express')
const userRouter = express.Router()
const User = require('../models/users')

userRouter.get('/', (req, res) => {
  res.send('User route')
})

userRouter.get('/:uuid', (req, res) => {
  User.find({ uuid: req.params.uuid }, (err, user) => {
    if (err) {
      console.log(err)
    }
    res.status(200).json({ user })
  })
})

module.exports = userRouter
