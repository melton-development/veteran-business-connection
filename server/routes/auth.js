const express = require('express')
const authRouter = express.Router()
const User = require('../models/users')
const passport = require('passport')
const bcrypt = require('bcryptjs')
require('../config/passport-config')

passport.serializeUser(function (user, done) {
  done(null, user)
})

passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user)
  })
})

authRouter.post('/login', (req, res) => {
  const password = req.body.password
  User.find({ email: req.body.email }, (err, user) => {
    if (err) console.log(err)
    if (user.length === 0) res.status(200).json({ status: 701, message: 'No User Found.' })
    bcrypt.compare(password, user[0].password, (err, result) => {
      if (err) console.log(err)
      if (result === true) {
        res.status(200).json({ user })
        if (result === false) {
          return res.status(200).json({ status: 702, message: 'Password not valid!' })
        }
      }
    })
  })
})

authRouter.post('/register', (req, res) => {
  bcrypt.hash(req.body.password, 10)
    .then(function (hashedPassword) {
      return User.findOrCreate({
        uuid: req.body.uuid,
        email: req.body.email,
        name: req.body.name,
        password: hashedPassword
      }, (err, user) => {
        if (err) {
          return res.json({ status: 500, message: 'Email is already in use. Please sign in.' })
        } else {
          res.status(200).json({ user: [user] })
        }
      })
    })
})

authRouter.get('/redirect', passport.authenticate('oauth2', { failureRedirect: '/' }), function (req, res, next) {
  res.redirect(`http://localhost:3000/register?token=${req.user.token}`)
})

module.exports = authRouter
