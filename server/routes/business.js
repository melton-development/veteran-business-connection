const express = require('express')
const businessRouter = express.Router()
const Business = require('../models/business')
require('../config/passport-config')

// Return all businesses.
businessRouter.get('/', (req, res) => {
  Business.find(function (err, businesses) {
    if (err) {
      console.log(err)
    } else {
      res.json({
        status: 302,
        message: 'All business Returned.',
        data: businesses
      })
    }
  })
})

// Find businesses by Owner.
businessRouter.get('/:id', (req, res) => {
  Business.findById({ _id: req.params.id }, function (err, businesses) {
    if (err) {
      console.log(err)
    } else {
      res.json({
        status: 302,
        message: 'Businesses found.',
        data: businesses
      })
    }
  })
})

// Find businesses by Owner.
businessRouter.get('/owner/:id', (req, res) => {
  Business.find({ ownerId: req.params.id }, function (err, businesses) {
    if (err) {
      console.log(err)
    } else {
      res.json({
        status: 302,
        message: 'Businesses found for owner.',
        data: businesses
      })
    }
  })
})

// Add Businesses. -- Verified in Postman.
businessRouter.post('/add', (req, res) => {
  Business.insertMany([{
    businessName: req.body.businessName,
    city: req.body.city,
    state: req.body.city,
    ownerName: req.body.ownerName,
    ownerId: req.body.ownerId,
    branchOfService: req.body.branchOfService,
    industry: req.body.industry,
    areasServiced: req.body.areasServiced,
    phone: req.body.phone,
    email: req.body.email,
    website: req.body.website
  }], (err, document) => {
    if (err) {
      console.log(err)
    } else {
      res.json({
        status: 201,
        message: 'Business added.',
        data: document
      })
    }
  })
})

// Edit Business Information. (by business ID) -- Verified in Postman.
businessRouter.put('/:id/edit', (req, res) => {
  Business.findByIdAndUpdate({ _id: req.params.id },
    {
      businessName: req.body.businessName,
      city: req.body.city,
      state: req.body.city,
      ownerName: req.body.ownerName,
      ownerId: req.body.ownerId,
      branchOfService: req.body.branchOfService,
      industry: req.body.industry,
      areasServiced: req.body.areasServiced,
      phone: req.body.phone,
      email: req.body.email,
      website: req.body.website
    }, (err, document) => {
      if (err) {
        console.log(err)
      } else {
        res.json({
          status: 204,
          message: 'Record Updated Successfully.'
        })
      }
    })
})

// Delete Business Information (by business ID) -- Verified in Postman.
businessRouter.delete('/remove', (req, res) => {
  Business.findByIdAndDelete({ _id: req.query.id }, (err, document) => {
    if (err) {
      console.log(err)
    } else {
      res.json({
        status: 200,
        message: 'Record Deleted.',
        data: document
      })
    }
  })
})

module.exports = businessRouter
