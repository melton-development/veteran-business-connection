const path = require('path')
require('dotenv').config({
  path: path.resolve(__dirname, '../../.env')
})
const passport = require('passport')
// const request = require('request')
const Oauth2 = require('passport-oauth2')

passport.use(new Oauth2({
  authorizationURL: process.env.AUTH_URL,
  tokenURL: process.env.ACCESS_TOKEN_URL,
  clientID: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  callbackURL: 'http://localhost:8080/api/v1a/auth/redirect',
  scope: process.env.OAUTH_SCOPE
},
async function (accessToken, refreshToken, profile, done) {
  profile = {
    token: accessToken
  }
  done(null, profile)
})
)
