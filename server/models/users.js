const mongoose = require('mongoose')
const Schema = mongoose.Schema
const supergoose = require('supergoose')

const UsersSchema = new Schema({
  uuid:
  {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
})

UsersSchema.plugin(supergoose)

module.exports = mongoose.model('Users', UsersSchema)
