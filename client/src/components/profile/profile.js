import React, { Component } from 'react';

import './profile.scss';
//eslint-disable-next-line
import { Field, reduxForm, handleSubmit } from 'redux-form';
import { connect } from 'react-redux';

class profile extends Component {
    state = {
        show: false,
        items: []}

    componentDidMount () {
        fetch('/businesses/owner/' + this.props.user.uuid)
            .then(results => {
                return results.json()
            }).then(data => {
                var items = data.data.map(item => {
                    return (
                    <div key={item._id}>
                        <h6>{item.businessName}</h6>
                    </div>)
                });
                this.setState({ items });
            });
    }
 
    submit = (values) => {

    }

    showForm = () => {
        this.setState({ show: true });
    }

    hideForm = () => {
        this.setState({ show: false })
    }

    render () {
        const { handleSubmit } = this.props
        return (
            <div>
                <h2>User Businesses:</h2>
                <ul>{this.state.items}</ul>
                <form className={this.state.show ? 'modal display-block' : 'modal display-none'} onSubmit={handleSubmit(this.submit.bind(this))}>
                        <Field name="busName" component="input" type="text" placeholder="Business Name" />
                        <br/>
                        <button type='submit'>Add</button>
                        <button onClick={this.hideForm}>Cancel</button>
                </form>
                <button onClick={this.showForm}>Add Business</button>
            </div>
        );
    }
}

export function mapStatetoProps (state) {
    return {
        user: state.user
    }
}

const reduxFormAddBusiness = reduxForm({ form: 'postBiz' })(profile);

export default connect(mapStatetoProps)(reduxFormAddBusiness);