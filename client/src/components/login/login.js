import React, { Component } from 'react';
//eslint-disable-next-line
import { Field, reduxForm, handleSubmit } from 'redux-form';
import { signInAction } from '../../actions';
import { connect } from 'react-redux';

import './login.scss';

class login extends Component {

    submit = (values) => {
        this.props.signInAction(values, this.props.history);
    }

    errorMessage () {
        if (this.props.errorMessage) {
            return (
                <div className="info-red">
                    {this.props.errorMessage}
                </div>
            );
        }
    }

    render () {
        const { handleSubmit } = this.props;
        return (
            <div className="loginForm">
                <div className="container">
                    <h2>Sign In</h2>
                    <form onSubmit={handleSubmit(this.submit.bind(this))}>
                        <Field className="field"
                            name="email"
                            component="input"
                            type="text"
                            placeholder="Email"
                        />
                        <br/>
                        <Field className="field"
                            name="password"
                            component="input"
                            type="password"
                            placeholder="Password"
                        />
                        <br/>
                        <button type="submit" className="blue">Sign In</button>
                    </form>
                    {this.errorMessage()}
                    <a href="/register">Need an Account?</a>
                </div>
            </div>
        )
    }   
}

function mapStateToProps (state) {
    return { errorMessage: state.auth.error || "" };
}

const reduxFormSignin =  reduxForm({ form: 'signin' })(login);

export default connect(mapStateToProps, {signInAction})(reduxFormSignin);