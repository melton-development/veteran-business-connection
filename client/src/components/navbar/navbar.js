import React, { useState } from 'react';
import { Navbar as NavbarElm, NavbarBrand, Nav, NavItem, Collapse, NavbarToggler } from 'reactstrap';
import { Link } from 'react-router-dom';

import './navbar.scss';

const authLink = (authState) => {
  if (authState) {
    return (<NavItem><Link to="/signout">Sign out</Link></NavItem>);
  }
  return (<NavItem><Link to="/login">Login / Register</Link></NavItem>);
}

const navbarLinks = (authState, user) => {
    if (authState) {
      return [
        <NavItem><Link key="directory" to="/directory">Directory</Link></NavItem>,
        <NavItem><Link key={`nav-${user.uuid}`} to={`/profile/${user.uuid}`}>Profile</Link></NavItem>,
      ];
    }
    return [
      <NavItem><Link key ="directory" to="/directory">Directory</Link></NavItem>
    ];
  }     

const Navbar = (props) => {
  const auth = props.auth;
  const user = props.user;
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
      <div>
        <NavbarElm expand="md">
          <NavbarBrand><Link to="/"><img id="logo" alt="VBC-logo" src="/VBC.png"/></Link></NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
             <Nav className="mr-auto" navbar>
              {navbarLinks(auth.authenticated, user)}
            </Nav>
            {authLink(auth.authenticated)}
          </Collapse>
        </NavbarElm>
      </div>
    );
}

export default Navbar;