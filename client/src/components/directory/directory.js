import React, { Component } from 'react';

import './directory.scss';

class directory extends Component {
    constructor() {
        super();
        this.state = {
            items: []
        }
    }
    
    componentDidMount () {
        fetch('/businesses')
            .then(results => {
                return results.json()
            }).then(data => {
                let items = data.data.map((item) => {
                    return (
                        <div key={item._id} className="list-item">
                            <div className="d-flex">
                            <div className="heading-fix">{item.name}</div>
                            </div>
                            <div className="collapse-items">
                                <div className="list-text">Business: {item.industry}</div>
                                <div className="list-text">Address: {item.address}</div>
                                <div className="list-text">{item.city}, {item.state} {item.zip}</div>
                                <div className="list-text">Website: {item.website}</div>
                            </div>
                    </div>
                    )
                });
                this.setState({ items: items });
            })
    }

    render() {
        return (
            <div>
                {this.state.items}
            </div>
        );
    }
}

export default directory;