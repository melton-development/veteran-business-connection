import React, { Component } from 'react';
import queryString from 'querystring';
//eslint-disable-next-line
import { Field, reduxForm, handleSubmit } from 'redux-form';
import { registerAction, fetchData, fetchPending  } from '../../actions';
import { connect } from 'react-redux';
import './register.scss';

class Register extends Component { 

    submit = (values) => {
        if (values.password === values.cpassword) {
            let obj = this.props.fetch.user;
            obj['password'] = values.password;
            this.props.registerAction(obj, this.props.history);
        }                                                                       
    }

    errorMessage () {
        if (this.props.errorMessage) {
            return (
                <div className="info-red">
                    {this.props.errorMessage}
                </div>
            );
        }
    }

    componentWillMount () {
        let values = queryString.parse(window.location.search)
        this.props.fetchPending();
        if ('?token' in values) {
            this.props.fetchData(values['?token']);
        }
    }

    render () {
        let values = queryString.parse(window.location.search)
        if ('?token' in values) {
            const { handleSubmit } = this.props
            return (
                <div>
                    <div className="display">
                        <form onSubmit={handleSubmit(this.submit.bind(this))}>
                            <Field name="password" component="input" type="password" placeholder="Password" />
                            <Field name="cpassword" component="input" ype="password" placeholder="Confirm Password" />
                            <button type="submit">Register</button>
                        </form>
                    </div>
                    <div className="displayNone">
                        <button>
                            <a href="https://api.id.me/oauth/authorize?client_id=509c639a853c325b3433f5f796b3b109&redirect_uri=http://localhost:8080/api/v1a/auth/redirect/&response_type=code&scope=military">
                                <img alt='idme-logo' src={process.env.PUBLIC_URL + '/idme-button.png'} />
                            </a>
                        </button>
                    </div>
                </div>
            );
        } else {
            const { handleSubmit } = this.props
            return (
                <div>
                    <div className="displayNone">
                        <form onSubmit={handleSubmit(this.submit.bind(this))}>
                            <Field name="password" component="input" type="password" placeholder="Password" />
                            <Field name="cpassword" component="input" type="password" placeholder="Confirm Password" />
                            <button type="submit">Register</button>
                        </form>
                    </div>
                    <div className="display">
                        <h1>Click Button Below to register!</h1>
                        <button>
                            <a href="https://api.id.me/oauth/authorize?client_id=509c639a853c325b3433f5f796b3b109&redirect_uri=http://localhost:8080/api/v1a/auth/redirect/&response_type=code&scope=military">
                                <img alt='idme-logo' src={process.env.PUBLIC_URL + '/idme-button.png'} />
                            </a>
                        </button>
                    </div>
                </div>
            );
        }

    }
}

function mapStateToProps (state) {
    return {
        fetch: state.fetch
    }
}

const reduxFormRegister = reduxForm({ form: 'register' })(Register);

export default connect(mapStateToProps, { fetchData, fetchPending, registerAction })(reduxFormRegister);