import * as actionCreators from '../actions';

import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import reducers from '../reducers';

function saveToLocalStorage (state) {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  } catch (e) {
    console.log(e);
  }
}

function loadFromLocalStorage () {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) return undefined
    return JSON.parse(serializedState);
  } catch (e) {
    console.log(e);
    return undefined
  }
}

const persistState = loadFromLocalStorage();
export let isMonitorAction;
const store = createStore(reducers, persistState, compose(applyMiddleware(reduxThunk), window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__({
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: (monitor) => { isMonitorAction = monitor.isMonitorAction; },
      actionCreators
  }) : f => f
))

store.subscribe(() => saveToLocalStorage(store.getState()));

export default store;