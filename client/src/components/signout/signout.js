import React, { Component } from 'react';
import { signOutAction } from '../../actions';
import { connect } from 'react-redux';


import './signout.scss';

class signout extends Component {

    componentWillMount () {
        this.props.signOutAction()
    }

    render() {
        return (
            <div>
                Successfully Signed Out.
            </div>
        );
    }
}

function mapStateToProps (state) {
    return { errorMessage: state.auth.error };
}

export default connect(mapStateToProps, {signOutAction})(signout);