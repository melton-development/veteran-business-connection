import React, { Component } from 'react';
import './App.scss';
import { Route } from 'react-router-dom';
//import Cookies from 'js-cookie';


//Redux
import { connect } from 'react-redux'

//Components
import Directory from '../components/directory/directory';
import About from '../components/about/about';
import Login from '../components/login/login';
import Profile from '../components/profile/profile';
import Register from '../components/register/register';
import Navbar from '../components/navbar/navbar';
import SignOut from '../components/signout/signout';

import { withRouter } from 'react-router-dom';

class App extends Component {

  render () {
    return(
            <div className="App">
              <Navbar auth={this.props.auth} user={this.props.user}/>
              <Route exact path='/' component={About} />
              <Route path="/directory" component={Directory} />
              <Route path="/about" component={About} />
              <Route path="/login" component={Login} />
              <Route path="/profile" component={Profile} />
              <Route path='/register' component={Register} />
              <Route path='/signout' component={SignOut} />
            </div>)
    }
}

export function mapStateToProps (state) {
    return {
      fetch: state.fetch,
      user: state.user,
      auth: state.auth
    }
}

export default withRouter(connect(mapStateToProps)(App));
