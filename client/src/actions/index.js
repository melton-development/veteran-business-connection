import axios from 'axios';

export const AUTHENTICATED = 'authenticated_user';
export const UNAUTHENTICATED = 'unauthenticated_user';
export const AUTHENTICATION_ERROR = 'authentication_error';

export const FETCH_USER_DATA_PENDING = 'fetch_user_data_pending';
export const FETCH_USER_DATA_SUCCESS = 'fetch_user_data_success';
export const FETCH_USER_DATA_ERROR = 'fetch_user_data_error';

export function fetchRegisterDataPending () {
  return {
    type: FETCH_USER_DATA_PENDING
  }
} 

export function fetchRegisterDataSuccess (userData) {
  return {
    type: FETCH_USER_DATA_SUCCESS,
    userData
  }
}

export function fetchRegisterDataFailure (error) {
  return {
    type: FETCH_USER_DATA_ERROR,
    error: error
  }
}


export function fetchPending () {
  return dispatch => {
    dispatch(fetchRegisterDataPending());
  }
}

export function fetchData(values) {
  return dispatch => {
    dispatch(fetchRegisterDataPending());
    fetch(`https://cors-anywhere.herokuapp.com/https://api.id.me/api/public/v3/attributes.json?access_token=${values}`)
      .then(res => {
        return res.json();
      })
      .then(res => {
        let user = {
          uuid: res.attributes[4].value,
          name: res.attributes[1].value + ' ' + res.attributes[2].value,
          email: res.attributes[0].value
        }
        dispatch(fetchRegisterDataSuccess(user))
        return user;
      })
      .catch(error => {
        dispatch(fetchRegisterDataFailure(error))
    })
  }
}

export function registerAction ({ uuid, email, name, password }, history) {
  return async (dispatch) => {
    try {
      if (uuid === undefined || email === undefined || name === undefined || password === undefined) {
        dispatch({
          type: AUTHENTICATION_ERROR,
          payload: 'All Fields Required.'
        });
      }
      const res = await axios.post('/auth/register', { uuid, email, name, password });
      if (res.data.status === 500) {
        dispatch({
          type: AUTHENTICATION_ERROR,
          payload: res.data.message
        });
        history.push('/login')
      } else {
        dispatch({ type: AUTHENTICATED, userdata: res.data.user[0] })
      }
    } catch (error) {
      console.log(error);
      dispatch({
        type: AUTHENTICATION_ERROR,
        payload: JSON.stringify(error)
      });
    }
  }
}


export function signInAction({ email, password }, history) {
  return async (dispatch) => {
    try {
      if (password === undefined || email === undefined) {
        dispatch({
          type: AUTHENTICATION_ERROR,
          payload: 'All Fields Required.'
        });
      }
      const res = await axios.post(`/auth/login`, { email, password });
      dispatch({ type: AUTHENTICATED, userdata: res.data.user[0] });
      history.push('/profile/' + res.data.user[0].uuid);
    } catch (error) {
      let resCode = error.message.slice(error.message.length - 3).toString(); 
      switch (resCode) {
        case '701':
          dispatch({
            type: AUTHENTICATION_ERROR,
            payload: 'User Not Found. Please Register.'
          });
          break;
        case '709':
          dispatch({
            type: AUTHENTICATION_ERROR,
            payload: 'Invalid Password.'
          });
          break;
        default:
          dispatch({
            type: AUTHENTICATION_ERROR,
            payload: 'Invalid Email or Password.'
          });
      }
    }
  };
}

export function signOutAction() {
  localStorage.clear();
  return {
    type: UNAUTHENTICATED
  };
}