import { AUTHENTICATED, UNAUTHENTICATED, AUTHENTICATION_ERROR } from '../actions';

export default function (state = {}, action) {
  switch (action.type) {
    case AUTHENTICATED:
      return { ...state, name: action.userdata.name, uuid: action.userdata.uuid, error: '' };
    case UNAUTHENTICATED:
      return { ...state, name: undefined, uuid: undefined, error: ''};
    case AUTHENTICATION_ERROR:
      return { ...state, error: action.payload };
    default:
      break;
  }
  return state;
}