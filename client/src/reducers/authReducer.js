import { AUTHENTICATED, UNAUTHENTICATED, AUTHENTICATION_ERROR } from '../actions';

export default function (state = {}, action) {
  switch (action.type) {
    case AUTHENTICATED:
      return { ...state, authenticated: true, error: '' };
    case UNAUTHENTICATED:
      return { ...state, authenticated: false, error: ''};
    case AUTHENTICATION_ERROR:
      return { ...state, error: action.payload };
    default:
      break;
  }
  return state;
}