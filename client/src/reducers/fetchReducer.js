import { FETCH_USER_DATA_ERROR, FETCH_USER_DATA_SUCCESS, FETCH_USER_DATA_PENDING, AUTHENTICATED } from '../actions';

export default function fetchReducer (state = {}, action) {
  switch (action.type) {
    case FETCH_USER_DATA_PENDING:
      return {
        ...state, 
        pending: true
      }
    case FETCH_USER_DATA_SUCCESS:
      return {
        ...state,
        pending: false,
        user: action.userData
      }
    case FETCH_USER_DATA_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error
      }
    case AUTHENTICATED:
      return {
        ...state,
        pending: false,
        user: undefined
      }
    default:
      return state;
  }
}